//POST para API
const url = "https://lobinhos.herokuapp.com/wolves/"
let form = document.querySelector("#add-wolf-form")
form.addEventListener("submit", e => {
  e.preventDefault()
  let wolfName = document.querySelector("#wolf-name-input").value
  let age = document.querySelector("#wolf-age-input").value
  let link = document.querySelector("#wolf-link-input").value
  let description = document.querySelector("#wolf-description-input").value

  let fetchBody = {
    "wolf": {
      "name": wolfName,
      "description": description,
      "link_image": link,
      "age": parseInt(age)
    }
  }

  let fetchConfig = {
    method: "POST",
    headers: {"Content-type": "application/json"},
    body: JSON.stringify(fetchBody) 
  }
  fetch(url, fetchConfig)
  .then (resp => resp.json())
  .then (resp => {
    alert("Parabéns, mais um lobinho para a alcateia!")
  })
})