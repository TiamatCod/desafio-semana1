//POST para API
const id = window.location.search.split("=")[1];
const urlAdopter = "https://lobinhos.herokuapp.com/wolves/adoption"
let form = document.querySelector("#adopter-submission")
form.addEventListener("submit", e => {
  // e.preventDefault()
  let adopterName = document.querySelector("#adopter-name").value
  let adopterAge = document.querySelector("#adopter-age").value
  let adopterEmail = document.querySelector("#adopter-email").value

  let fetchBody = {
    "adoption": {
      "name": adopterName,
      "email": adopterEmail,
      "age": parseInt(adopterAge),
      "wolf_id": parseInt(id)
    }
  }

  console.log(fetchBody)

  let fetchConfig = {
    method: "POST",
    headers: {"Content-type": "application/json"},
    body: JSON.stringify(fetchBody) 
  }
  fetch(urlAdopter, fetchConfig)
  .then (resp => resp.json())
  .then (resp => {
    alert("Parabéns, essa foi uma grande escolha!")
    urlShow = "list-wolves.html"
    console.log(urlShow)
    window.open(urlShow,"_SELF")
  })
})