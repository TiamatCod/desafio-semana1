const queryString = window.location.search.split("=")[1];

const url = "https://lobinhos.herokuapp.com/wolves/"
//GET para API pelo ID
fetch(url+queryString)
.then(resp => resp.json()
.then (resp =>{
  console.log(resp)
  adoptionCard(resp)
}))
.catch(err => console.log(err))

function adoptionCard(element){
  let img = document.querySelector("#wolf-image-adoption") 
  img.src = element.link_image
  let h1 = document.querySelector("h1")
  h1.innerText = "Adote o(a) " + element.name
}

