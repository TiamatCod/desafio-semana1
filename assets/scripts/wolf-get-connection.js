function loadPage(){
  const url = "https://lobinhos.herokuapp.com/wolves/"
  fetch(url)
  .then(resp => resp.json()
  .then (elements =>{
    let contador = 0
    //elements = elements.slice(0 ,20)
    elements.forEach (element => {
      createCards(element, contador)
      contador++;
    })
    setPagination(1)
    
  }))
  .catch(err => console.log(err))
}
loadPage()