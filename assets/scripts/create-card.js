function createCards (message, indice){
  const itensPagina = 4
  let pagina = Math.floor(indice/itensPagina)+1
    
  if (indice%itensPagina==0){
    createPagination(pagina)
  }
  let div_container = document.querySelector("#container-list-wolves")
  let div_example_wolves = document.createElement('div');
  let div_link_wolves = document.createElement('div');
  let div_description_wolves = document.createElement('div');
  let show_button = document.createElement('button');
  let id  = message.id
  show_button.innerText = "Adotar"
  show_button.id = "adopt-wolf-" + id
  let p_description = document.createElement('p');
  let div_description = document.createElement('div');
  let h3 = document.createElement('h3');
  let p_age = document.createElement('p')
  
  // Não funciona não sei porquê - agora funfou
  show_button.addEventListener("click", e => {
    e.preventDefault()
    urlShow = "show-wolf.html?id=" + e.target.id.split("-")[2]
    
    window.open(urlShow,"_SELF")
  })
  div_example_wolves.classList.add("example-wolves")
  div_example_wolves.classList.add("pagina-" + pagina.toString())
  
  div_link_wolves.classList.add("wolf-image")
  div_link_wolves.style.backgroundImage = "url('" + message.link_image + "')";
  div_example_wolves.append(div_link_wolves)
  div_description_wolves.classList.add("wolf-description")
  h3.innerText = message.name
  p_age.innerText = "Idade " + message.age + " anos"
  p_description = message.description
  div_description_wolves.append(h3)
  div_description_wolves.append(p_age)
  div_description_wolves.append(show_button)
  div_description_wolves.append(p_description)
  div_example_wolves.append(div_description_wolves)
  div_container.append(div_example_wolves)
  
}

function createCard (message){
  let div_container = document.querySelector("#show-wolf-container")
  let div_show = document.createElement("div")
  div_show.classList.add("show-wolf")
  let div_img = document.createElement('div')
  div_img.innerHTML = "<img src='" + message.link_image + "'alt=''></img>"
  
  let div_description = document.createElement('div')
  div_description.innerHTML = "<p>" + message.description + "</p>"


  let adopt_button = document.createElement('button')
  let remove_button = document.createElement('button')
  id = message.id
  adopt_button.id = "show-adopt-wolf-"+id
  remove_button.id = "show-remove-wolf-"+id
  adopt_button.addEventListener("click",e=>{
    e.preventDefault()
    urlShow = "adopt.html?id=" + e.target.id.split("-")[3]
   
    window.open(urlShow,"_SELF")
  })
  remove_button.addEventListener("click",e=>{
    e.preventDefault()
    removeWolf(e.target.id)
    urlShow = "list-wolves.html"
   
    window.open(urlShow,"_SELF")
  })
  adopt_button.innerText = "Adotar"
  remove_button.innerText = "Excluir"
  div_show.append(div_img)
  div_show.append(adopt_button)
  div_show.append(remove_button)
  div_container.append(div_show)
  div_container.append(div_description)
}

function clearCards(){
  lobos = document.querySelectorAll(".example-wolves")
  lobos.forEach(element =>{
    element.remove()
  })
}

function createPagination(pagina){
  let div_pagination = document.querySelector(".pagination-container")
  let paginacao = document.createElement("button")
  paginacao.classList.add("pagina")
  paginacao.classList.add(pagina.toString())
  paginacao.innerText = pagina.toString()
  paginacao.addEventListener("click", e => {
    // console.log(e.target.innerText)
    selectPage(e.target)
    showPage(e.target.innerText)
    setPagination(e.target.innerText)
  })
  div_pagination.append(paginacao)
}

function selectPage(element){
  let items = document.querySelectorAll(".selected")
  items.forEach(item => {
    item.classList.remove("selected")
  })
  element.classList.add("selected")
}

function setPagination(indice){
  let items = document.querySelectorAll(".pagina")
  let primeiro = 1
  let ultimo = items.length
  // console.log(typeof(indice))
  let numeroPaginas = 5
  let min = 0
  let max = 0
  
  if (indice < primeiro + 2){
    min = indice
    max =  parseInt(numeroPaginas) 
    // console.log(min + "a" + max)
  } else if (indice > ultimo - 2){
    max = indice
    min = parseInt(max) - parseInt(numeroPaginas) + 1
    // console.log(min + "b" + max)
  } else {
    max = parseInt(indice) + 2
    min = parseInt(indice) - 2
    // console.log(min + "c" + max)
  }
  // console.log(min + "-" + max)
  // let min = parseInt(indice,10) - 2 
  // let max = parseInt(indice,10) + 2
  // console.log(min + "-" + max)
  // // let div_pagination = document.querySelector(".pagination-container")
  
  items.forEach(item => {
    let page = parseInt(item.innerHTML,10)
    // console.log(page)
    
    if ((page >= min && page <= max) || (page == primeiro) || (page == ultimo)){
      // console.log(item.innerText)
      item.style.display = "inline-block"
    } else {
      item.style.display = "none"
    }
  })
}

function showPage(pagina){
  hidePage("example-wolves")
  let cards = document.querySelectorAll(".example-wolves.pagina-" + pagina)
  // console.log(cards)
  cards.forEach(card => {
    // console.log(cards.values)
    card.style.display = "flex"
  })
}

function hidePage(classe){
  selector = "." + classe
  let cards = document.querySelectorAll(selector)
  cards.forEach(card => {
    card.style.display = "none"
  })
}
