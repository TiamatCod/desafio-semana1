function removeWolf(id){
  fetch('https://lobinhos.herokuapp.com/wolves/' + id, {
    method: 'DELETE',
  })
  .then(res => res.text()) // or res.json()
  .then(res => console.log(res))
}